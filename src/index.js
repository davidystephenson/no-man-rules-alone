import React from 'react'
import ReactDOM from 'react-dom'
import App from './view/App'
import Provider from './context/fire/Provider'

ReactDOM.render(
  <Provider>
    <App />
  </Provider>,
  document.getElementById('root')
)
