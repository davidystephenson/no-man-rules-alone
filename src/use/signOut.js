import { useContext } from 'react'

import useAction from '../use/action'

import fireContext from '../context/fire'

export default function useSignOut () {
  const { auth } = useContext(fireContext)

  async function action () {
    return auth.signOut()
  }

  return useAction(action)
}
