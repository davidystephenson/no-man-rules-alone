import useAction from '../action'
import useAuthForm from './form'

export default function useAuthAction (
  action
) {
  const {
    email,
    name,
    password,
    setEmail,
    setName,
    setPassword
  } = useAuthForm()

  async function authAction () {
    return action(email, name, password)
  }

  const {
    act, done, failed, loading, status
  } = useAction(authAction)

  return {
    act,
    done,
    email,
    failed,
    loading,
    name,
    password,
    setEmail,
    setName,
    setPassword,
    status
  }
}
