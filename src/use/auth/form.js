import { useState } from 'react'

export default function useAuthForm () {
  const [email, setEmail] = useState('')
  const [name, setName] = useState('')
  const [
    password, setPassword
  ] = useState('')

  return {
    email,
    name,
    password,
    setEmail,
    setName,
    setPassword
  }
}
