import { useContext } from 'react'
import { useAuthState } from 'react-firebase-hooks/auth'

import fireContext from '../../context/fire'
import userContext from '../../context/user'

export default function useAuth () {
  const { auth } = useContext(fireContext)
  const context = useContext(userContext)

  const state = useAuthState(auth)
  const [user, loading] = state

  const authed = user &&
    user.uid &&
    context &&
    user.uid === context.uid

  return {
    authed,
    loading,
    state,
    user
  }
}
