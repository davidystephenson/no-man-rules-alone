import { useContext } from 'react'

import gameContext from '../context/game'
import playContext from '../context/play'

export default function useAgendas (player) {
  const {
    agendas, achieved
  } = useContext(gameContext)
  const {
    player: current
  } = useContext(playContext)

  player = player || current

  const cards = Object.keys(agendas)

  const hand = []
  const done = []

  const mine = cards.filter(card => {
    const agenda = agendas[card]

    return agenda && agenda.id === player.id
  })
  mine.forEach(card => {
    const achievement = achieved[card]

    if (achievement) {
      return done.push(card)
    }

    return hand.push(card)
  })

  return {
    cards, done, mine, hand
  }
}
