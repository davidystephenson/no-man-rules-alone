import { useContext } from 'react'

import fireContext from '../context/fire'

export default function useRef (
  ref, type
) {
  const { store } = useContext(fireContext)

  const string = typeof ref === 'string'

  return string
    ? store[type](ref)
    : ref
}
