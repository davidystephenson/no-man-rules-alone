import React from 'react'

import fireContext from '../context/fire'

const { useContext, useState } = React

export default function useFunction (
  name,
  { data, start, end, label, onDone, set }
) {
  const {
    functions
  } = useContext(fireContext)

  const [first] = name
  const lower = name
    .replace(first, first.toLowerCase())

  label = label || name

  start = start || `${label}ing...`
  end = end || `${label}ed!`

  const [status, setStatus] = useState('')
  const [
    loading,
    setLoading
  ] = useState(false)

  const [failed, setFailed] = useState(false)
  const [done, setDone] = useState(false)

  async function run (event) {
    if (event) {
      event.preventDefault()
    }

    setStatus(start)
    setLoading(true)
    setDone(false)
    setFailed(false)

    const call = functions.httpsCallable(lower)

    try {
      const result = await call(data)

      setStatus(end)
      setLoading(false)
      setDone(true)

      onDone && onDone()

      if (set) {
        set(null)
      }

      return result
    } catch (error) {
      setStatus(error.message)
      setLoading(false)
      setFailed(true)

      throw error
    }
  }

  return {
    [lower]: run,
    done,
    failed,
    loading,
    run,
    setStatus,
    status
  }
}
