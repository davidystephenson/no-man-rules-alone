import { useState } from 'react'

export default function useAction (action) {
  const [done, setDone] = useState(false)
  const [failed, setFailed] = useState(false)
  const [loading, setLoading] = useState(false)
  const [status, setStatus] = useState('')

  async function act () {
    setDone(false)
    setFailed(false)
    setLoading(true)
    setStatus('Signing in...')

    try {
      await action()

      setDone(true)
      setLoading(false)
      setStatus('Signed in!')
    } catch (error) {
      console.error(error)

      setFailed(true)
      setLoading(false)
      setStatus(error.message)
    }
  }

  return {
    act,
    done,
    failed,
    loading,
    status
  }
}
