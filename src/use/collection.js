import {
  useCollectionData
} from 'react-firebase-hooks/firestore'

import useRef from './ref'

export default function useCollection (
  collection
) {
  const ref = useRef(collection, 'collection')

  return useCollectionData(
    ref, { idField: 'id' }
  )
}
