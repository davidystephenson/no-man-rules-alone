import {
  useDocumentData
} from 'react-firebase-hooks/firestore'

import useRef from './ref'

export default function useDoc (doc) {
  const ref = useRef(doc, 'doc')

  return useDocumentData(
    ref, { idField: 'id' }
  )
}
