import { useContext } from 'react'
import fireContext from '../context/fire'
import useCollection from './collection'

export default function useWhere (
  collection, a, versus, b
) {
  const { store } = useContext(fireContext)

  const query = store
    .collection(collection)
    .where(a, versus, b)

  return useCollection(query)
}
