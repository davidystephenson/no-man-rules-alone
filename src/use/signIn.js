import { useContext } from 'react'

import useAuthAction from '../use/auth/action'

import fireContext from '../context/fire'

export default function useSignIn () {
  const { auth } = useContext(fireContext)

  async function action (
    email, name, password
  ) {
    return auth.signInWithEmailAndPassword(
      email, password
    )
  }

  return useAuthAction(action)
}
