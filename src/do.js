export function alphabetize (a, b) {
  a = a.uid.toLowerCase()
  b = b.uid.toLowerCase()

  if (a > b) return 1

  return -1
}
