import React from 'react'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/functions'
import context from './index.js'
import config from './config.js'

export default function Provider ({
  children
}) {
  const [fire, setFire] = React.useState({})

  if (!firebase.apps.length) {
    const app = firebase.initializeApp(config)
    const auth = app.auth()
    const store = app.firestore()
    const functions = firebase.functions()

    const { NODE_ENV } = process.env
    const develop = NODE_ENV === 'development'

    const { hostname } = window.location
    const local = hostname === 'localhost'

    const emulated = develop || local

    if (emulated) {
      console.warn('Firebase functions must be locally emulated.')
      functions._url = function (name) {
        return `http://localhost:5001/no-man-rules-alone/us-central1/${name}`
      }
    }

    setFire({ app, auth, store, functions })
  }

  return <context.Provider value={fire}>
    {children}
  </context.Provider>
}
