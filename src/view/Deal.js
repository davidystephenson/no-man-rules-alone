import React from 'react'

import Exchange from './Exchange'
import Pledged from './Pledged'

export default function Deal ({ where }) {
  return <Exchange
    collection='influence'
    View={Pledged}
    where={where}
  />
}
