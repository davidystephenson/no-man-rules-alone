import React, { useContext } from 'react'

import gameContext from '../context/game'
import playContext from '../context/play'

import Function from './Function'

export default function Discard ({
  hand, name, points
}) {
  const {
    id, phase, turn
  } = useContext(gameContext)
  const { player } = useContext(playContext)

  const data = { id, name }

  const discard = <Function
    name='Discard' data={data}
  />

  if (phase === 'draft' && hand.length > 3) {
    return discard
  }

  const mine = turn && turn.id === player.id

  if (mine) {
    if (phase === 'discard') {
      return discard
    }

    const phases = ['victory', 'achieved']

    if (phases.includes(phase)) {
      const data = { id, name, points }

      return <Function
        name='Achieve'
        start='Achieving...'
        end='Achieved!'
        data={data}
      />
    }
  }

  return null
}
