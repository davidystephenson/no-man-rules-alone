import React from 'react'

import SignOut from './SignOut'
import useAuth from '../use/auth'

export default function User () {
  const { user: { displayName } } = useAuth()

  return <SignOut>{displayName}</SignOut>
}
