import React from 'react'

import agendas from '../agendas'

import Card from './Card'
import Discard from './Discard'

export default function Agenda ({
  hand, name
}) {
  const agenda = agendas[name]

  if (!agenda) {
    console.warn('Agenda not found:', name)
    return null
  }

  const [
    quip, points, ...conditions
  ] = agenda

  const title = <h3>{name} ({points})</h3>

  const items = conditions
    .map(condition => <p key={condition}>
      {condition}
    </p>)

  return <Card>
    {title}

    <i>{quip}</i>

    {items}

    <Discard
      hand={hand} name={name} points={points}
    />
  </Card>
}
