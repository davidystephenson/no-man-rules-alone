import React, { useContext } from 'react'

import fireContext from '../context/fire'
import gameContext from '../context/game'

import useCollection from '../use/collection'

import Influence from './Influence'
import Viewer from './Viewer'

export default function Influences ({
  hand = false, player: { id: playerId }, debug
}) {
  const { store } = useContext(fireContext)
  const { gameRef } = useContext(gameContext)

  const playerRef = store
    .collection('player')
    .doc(playerId)

  if (debug) {
    console.debug('playerId test:', playerId)
    console.debug('playerRef test:', playerRef)
    console.debug('hand test:', hand)
    console.debug('gameRef test:', gameRef)
  }

  const influence = store
    .collection('influence')
    .where('owner', '==', playerRef)
    .where('hand', '==', hand)
    .where('game', '==', gameRef)

  const stream = useCollection(influence)

  if (debug) {
    console.debug('stream test:', stream)
  }

  return <Viewer
    stream={stream}
    View={Influence}
  />
}
