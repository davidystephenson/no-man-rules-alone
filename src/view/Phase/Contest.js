import React, { Fragment } from 'react'

import Deals from '../Deals'
import Tableaus from '../Tableaus'

export default function Contest () {
  return <Fragment>
    <h3>Contest</h3>

    <Deals />

    <Tableaus />
  </Fragment>
}
