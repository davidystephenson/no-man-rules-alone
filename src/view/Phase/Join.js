import React, { useContext } from 'react'

import playContext from '../../context/play'

import Move from '../Move'

export default function Join () {
  const {
    player, players
  } = useContext(playContext)

  if (!player) {
    return <Move name='Join' />
  }

  if (players.length > 3) {
    return <Move name='Start' />
  }

  return 'Waiting for more players'
}
