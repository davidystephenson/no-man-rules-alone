import React, {
  Fragment, useContext
} from 'react'

import gameContext from '../../context/game'
import playContext from '../../context/play'

import Function from '../Function'
import Deploy from '../Deploy'

export default function Freedom () {
  const {
    freedom, homeland, id, left, right
  } = useContext(gameContext)
  const {
    id: playerId
  } = useContext(playContext)

  // TODO amount -> armies
  const data = { id, amount: 0 }

  const minister = freedom.id === playerId
  const pass = minister && <Function
    data={data}
    name='Freedom'
    label='Pass'
    start='Passing...'
    end='Passed!'
  />

  const deploy = <table>
    <tr>
      <Deploy name='left' armies={left} />
      <Deploy
        name='homeland' armies={homeland}
      />
      <Deploy name='right' armies={right} />
    </tr>
  </table>

  return <Fragment>
    <h3>Freedom</h3>

    {pass}

    {deploy}
  </Fragment>
}
