import React, { Fragment } from 'react'

import Patrons from '../Patrons'
import Patronized from '../Patronized'

export default function Government () {
  return <Fragment>
    <h3>Patronage</h3>

    <Patronized />

    <Patrons />
  </Fragment>
}
