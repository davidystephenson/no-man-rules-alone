import React, {
  Fragment, useContext
} from 'react'

import { alphabetize } from '../../do'

import gameContext from '../../context/game'
import playContext from '../../context/play'

import Candidate from '../Candidate'
import Tableaus from '../Tableaus'

export default function Tie () {
  const { winners } = useContext(gameContext)
  const { players } = useContext(playContext)

  const titles = ['Leader']

  const candidates = winners.map(winner =>
    players
      .find(player => player.id === winner.id)
  )
    .sort(alphabetize)
    .map(player => <Candidate
      key={player.id}
      player={player}
      titles={titles}
    />)

  return <Fragment>
    <h3>Tie</h3>

    <Tableaus />

    <h4>Candidates</h4>

    {candidates}
  </Fragment>
}
