import React, { useContext } from 'react'

import playContext from '../../context/play'

import Move from '../Move'

export default function Lead () {
  const { current } = useContext(playContext)

  if (!current) {
    return null
  }

  return <Move end='Led!' name='Lead' />
}
