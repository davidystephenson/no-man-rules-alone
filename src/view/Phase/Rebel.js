import React, {
  Fragment, useContext
} from 'react'

import gameContext from '../../context/game'

import Patrons from '../Patrons'
import Support from '../Support'

export default function Rebel () {
  const {
    id
  } = useContext(gameContext)

  // TODO supporter value
  const support = {
    data: { id, support: 'Supporter' }
  }

  const rebel = {
    data: { id, support: 'Rebel' },
    end: 'Rebelled!',
    start: 'Rebelling...'
  }

  const answers = [support, rebel].map(
    answer => <Support
      key={answer.support} {...answer}
    />
  )

  return <Fragment>
    <h3>Rebellion</h3>

    <Patrons />

    {answers}
  </Fragment>
}
