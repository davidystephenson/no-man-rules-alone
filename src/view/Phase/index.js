import React, { useContext } from 'react'

import gameContext from '../../context/game'

import Achieved from './Achieved'
import Campaign from './Campaign'
import Contest from './Contest'
import Discard from './Discard'
import Draft from './Draft'
import Freedom from './Freedom'
import Government from './Government'
import Join from './Join'
import Lead from './Lead'
import Patronage from './Patronage'
import Peace from './Peace'
import Plenty from './Plenty'
import Rebel from './Rebel'
import Tax from './Tax'
import Tie from './Tie'
import Victory from './Victory'

export default function Phase () {
  const { phase } = useContext(gameContext)

  const phases = {
    achieved: Achieved,
    campaign: Campaign,
    contest: Contest,
    discard: Discard,
    draft: Draft,
    freedom: Freedom,
    government: Government,
    join: Join,
    lead: Lead,
    patronage: Patronage,
    peace: Peace,
    plenty: Plenty,
    rebel: Rebel,
    tax: Tax,
    tie: Tie,
    victory: Victory
  }

  const Phase = phases[phase]

  const content = Phase
    ? <Phase />
    : phase || '???'

  return <div className='phase'>{content}</div>
}
