import React, {
  Fragment, useContext
} from 'react'

import gameContext from '../../context/game'

import Remove from '../Remove'

export default function Tax () {
  const {
    balance, homeland, left, right
  } = useContext(gameContext)

  const number = balance * -1

  return <Fragment>
    <h3>Remove {number}</h3>

    <table>
      <tr>
        <Remove name='left' armies={left} />
        <Remove
          name='homeland' armies={homeland}
        />
        <Remove name='right' armies={right} />
      </tr>
    </table>
  </Fragment>
}
