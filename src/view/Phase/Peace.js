import React, {
  Fragment, useContext
} from 'react'

import gameContext from '../../context/game'
import playContext from '../../context/play'

import Function from '../Function'
import Operation from '../Operation'

export default function Peace () {
  const {
    homeland, id, left, right, peace
  } = useContext(gameContext)
  const {
    id: playerId
  } = useContext(playContext)

  const minister = peace.id === playerId

  // TODO amount -> armies
  const data = { id, amount: 0 }
  const pass = minister && <Function
    data={data}
    name='Peace'
    label='Pass'
    start='Passing...'
    end='Passed!'
  />

  return <Fragment>
    <h3>Peace</h3>

    {pass}

    <table>
      <thead>
        <tr>
          <th>{left}</th>
          <th>{homeland}</th>
          <th>{right}</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <Operation
              armies={left}
              from='left'
              to='homeland'
              right
            />
          </td>
          <td className='operations'>
            <Operation
              armies={homeland}
              from='homeland'
              to='left'
            />
            <Operation
              armies={homeland}
              from='homeland'
              to='right'
              right
            />
          </td>
          <td>
            <Operation
              armies={right}
              from='right'
              to='homeland'
            />
          </td>
        </tr>
      </tbody>
    </table>
  </Fragment>
}
