import React, {
  Fragment, useContext
} from 'react'

import gameContext from '../../context/game'
import playContext from '../../context/play'

import Function from '../Function'

export default function Plenty () {
  const {
    id, plenty, tax
  } = useContext(gameContext)
  const {
    id: playerId
  } = useContext(playContext)

  const minister = plenty.id === playerId

  const raise = {
    end: 'Raised!',
    name: 'Raise',
    change: 1,
    start: 'Raising...'
  }
  const pass = { name: 'Pass', change: 0 }
  const lower = { name: 'Lower', change: -1 }

  const options = [pass]

  if (tax <= 4) {
    options.push(raise)
  }

  if (tax >= 0) {
    options.push(lower)
  }

  const functions = minister && options.map(
    ({ name, change, end, start }) => {
      const data = { id, change }

      return <Function
        data={data}
        key={name}
        name='Plenty'
        label={name}
        end={end}
        start={start}
      />
    }
  )

  return <Fragment>
    <h3>Plenty</h3>

    {functions}
  </Fragment>
}
