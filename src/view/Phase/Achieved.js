import React, {
  Fragment, useContext
} from 'react'

import gameContext from '../../context/game'
import playContext from '../../context/play'

import Function from '../Function'

export default function Achieved () {
  const { id, turn } = useContext(gameContext)
  const {
    id: playerId, players
  } = useContext(playContext)

  const mine = turn && turn.id === playerId

  const data = { id }

  const done = mine && <Function
    data={data} name='Done' start='...' end='!'
  />

  const player = turn && players
    .find(player => player.id === turn.id)

  const name = player && player.name

  return <Fragment>
    <h3>Victory</h3>

    <h4>{name}</h4>

    {done}
  </Fragment>
}
