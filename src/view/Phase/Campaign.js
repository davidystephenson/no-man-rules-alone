import React, {
  Fragment, useContext
} from 'react'

import playContext from '../../context/play'

import Contribution from '../Contribution'

export default function Rebel () {
  const { player } = useContext(playContext)

  if (!player) {
    return null
  }

  return <Fragment>
    <h3>Campaign</h3>

    <p>{player.campaign}</p>

    <div className='inline'>
      <Contribution />
    </div>
  </Fragment>
}
