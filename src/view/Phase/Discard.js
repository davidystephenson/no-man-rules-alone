import React, {
  Fragment, useContext
} from 'react'

// TODO turn component
import gameContext from '../../context/game'
import playContext from '../../context/play'

export default function Discard () {
  const {
    turn
  } = useContext(gameContext)

  const {
    players
  } = useContext(playContext)

  const player = turn && players
    .find(player => player.id === turn.id)

  const name = player && player.name

  return <Fragment>
    <h3>Discard</h3>

    <h4>{name}</h4>
  </Fragment>
}
