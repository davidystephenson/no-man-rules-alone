import React, {
  Fragment, useContext
} from 'react'

import gameContext from '../../context/game'
import playContext from '../../context/play'

import Function from '../Function'

export default function Victory () {
  const {
    id, turn
  } = useContext(gameContext)

  const {
    id: playerId, players
  } = useContext(playContext)

  const mine = turn && turn.id === playerId

  const data = { id }

  const draw = mine && <Function
    data={data} name='Draw' end='Drawn!'
  />

  const player = players
    .find(player => player.id === turn.id)

  const name = player && player.name

  return <Fragment>
    <h3>Victory</h3>

    <h4>{name}</h4>

    {draw}
  </Fragment>
}
