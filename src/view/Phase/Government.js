import React, {
  Fragment, useContext
} from 'react'

import gameContext from '../../context/game'
import playContext from '../../context/play'

import Candidate from '../Candidate'

export default function Government () {
  const { players } = useContext(playContext)
  const game = useContext(gameContext)

  const ministries = [
    'Freedom', 'Peace', 'Plenty'
  ]

  const lowers = ministries
    .map(title => title.toLowerCase())

  const titles = ministries
    .filter((ministry, index) => {
      const lower = lowers[index]

      return !game[lower]
    })

  const candidates = players
    .map(player => <Candidate
      key={player.id}
      player={player}
      titles={titles}
    />)

  return <Fragment>
    <h3>Government</h3>

    {candidates}
  </Fragment>
}
