import React, { useContext } from 'react'

import playContext from '../context/play'

import Hand from './Hand'

export default function Area () {
  const { player } = useContext(playContext)

  if (player) {
    return <Hand key='hand' />
  }

  return null
}
