import React, { Fragment } from 'react'

import Auth from './Auth'
import Title from './Title'

export default function Header () {
  return <Fragment>
    <Title>No Man Rules Alone</Title>

    <Auth />
  </Fragment>
}
