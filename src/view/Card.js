import React from 'react'

export default function Card ({
  children, inline
}) {
  const classNames = ['card']

  if (inline) {
    classNames.push('inline')
  }

  const className = classNames.join(' ')

  return <div>
    <div className={className}>
      {children}
    </div>
  </div>
}
