import React, { useContext } from 'react'

import gameContext from '../context/game'

import Function from './Function'

export default function Pledge ({
  id, show, to
}) {
  const { phase } = useContext(gameContext)

  const data = { id, to: to.id }

  return phase === 'contest' && <Function
    data={data}
    end='Offered!'
    label={to.name}
    name='Pledge'
    start='Offering...'
    show={show}
  />
}
