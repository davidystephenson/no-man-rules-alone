import React from 'react'

import useAgendas from '../use/agendas'

import Agenda from './Agenda'

export default function Agendas () {
  const { hand } = useAgendas()

  return hand.map(agenda => <Agenda
    hand={hand} key={agenda} name={agenda}
  />)
}
