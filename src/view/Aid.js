import React from 'react'

export default function Aid () {
  return <div className='card aid'>
    <h4>Contest</h4>
    <p>
      Pledge to decide the next Leader
    </p>

    <h4>Government</h4>
    <p>The Leader appoints Ministers</p>

    <h5>Peace</h5>
    <p>
      You may move any number of armies from a neighbor to the Homeland, or from the Homeland to a neighbor
    </p>

    <h5>Freedom</h5>
    <p>
      You may raise up to 3 armies in the homeland or remove 1 army from anywhere.
    </p>

    <h5>Plenty</h5>
    <p>
      You may move the tax rate up or down by 1.
    </p>

    <h4>Victory</h4>
    <p>
      Starting with the Leader and proceeding alphabetically, each player either:
    </p>

    <p>
      - Achieves any number of agendas from their hand, or
    </p>

    <p>
      - Draws a new agenda, then discards an agenda
    </p>

    <h4>Tax</h4>
    <p>
      Each supporter pays the tax rate to the treasury.
    </p>

    <p>
      Then, the treasury pays 1 treasure for each army on the map.
    </p>

    <p>
      The Leader disbands unpaid armies.
    </p>

    <h4>Patronage</h4>
    <p>
      The Leader may pledge 10 treasure to other players.
    </p>

    <h4>Support</h4>
    <p>
      Players choose to be a Rebel or a Supporter.
    </p>

    <p>
      If the number of Rebels equals or exceeds armies in the homeland, return all pledged treasure to the supply.
    </p>

    <p>
      Otherwise, no one is a Rebel and players get the treasure pledged to them.
    </p>

    <h4>Campaign</h4>
    <p>
      Players bid any amount of treasure.
    </p>

    <p>
      The highest bidder takes 2 influence.
    </p>
  </div>
}
