import React, { useContext } from 'react'

import gameContext from '../context/game'
import playContext from '../context/play'

import Function from './Function'

export default function Operation ({
  armies, from, to, right
}) {
  const { id, peace } = useContext(gameContext)
  const {
    id: playerId
  } = useContext(playContext)

  const minister = peace.id === playerId

  const range = Array.from(
    { length: armies }, (index, x) => x + 1
  )

  const operations = minister &&
    range.map(value => {
      const data = {
        id, from, to, armies: value
      }

      const label = right
        ? `${value} >`
        : `< ${value}`

      return <Function
        data={data}
        key={value}
        name='Peace'
        label={label}
        start='Moving...'
        end='Moved!'
      />
    })

  return <div>{operations}</div>
}
