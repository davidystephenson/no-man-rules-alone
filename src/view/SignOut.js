import React from 'react'

import useSignOut from '../use/signOut'

import Form from './Form'

export default function SignOut ({
  children
}) {
  const {
    act,
    done,
    failed,
    loading,
    status
  } = useSignOut()

  return <Form
    done={done}
    failed={failed}
    loading={loading}
    name='Sign out'
    onSubmit={act}
    status={status}
  >
    {children}
  </Form>
}
