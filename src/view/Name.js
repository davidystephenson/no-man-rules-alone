import React, {
  Fragment, useContext
} from 'react'

import playerContext from '../context/player'
import gameContext from '../context/game'

import useAuth from '../use/auth'

import SignOut from './SignOut'

export default function Name () {
  const { user: { uid } } = useAuth()

  const {
    name, id
  } = useContext(playerContext)
  const game = useContext(gameContext)

  const titles = [
    'Freedom',
    'Leader',
    'Peace',
    'Plenty'
  ]

  const title = titles.find(title => {
    const lower = title.toLowerCase()

    return game[lower] &&
      game[lower].id === id
  })

  const label = title
    ? title === 'Leader'
      ? title
      : `Minister of ${title}`
    : null

  const official = <Fragment>
    <span>{label}</span> <b>{name}</b>
  </Fragment>

  if (uid === name) {
    return <SignOut>{official}</SignOut>
  }

  return official
}
