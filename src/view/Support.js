import React, { useContext } from 'react'

import playContext from '../context/play'

import Function from './Function'

export default function Support ({
  data, end, start
}) {
  const { player } = useContext(playContext)

  if (player) {
    return <Function
      data={data}
      name='support'
      label={data.support}
      start={start}
      end={end}
    />
  }

  return null
}
