import React, { Fragment } from 'react'

import Create from './Create'
import Link from './Link'
import User from './User'
import Viewer from './Viewer'

import useCollection from '../use/collection'

export default function Games () {
  const games = useCollection('game')

  return <Fragment>
    <User />

    <Create />

    <Viewer stream={games} View={Link} />
  </Fragment>
}
