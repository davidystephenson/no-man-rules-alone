import React, { useContext } from 'react'

import playContext from '../context/play'

import Card from './Card'
import Function from './Function'

export default function Offered ({
  amount, giver, id, mine, from, taker
}) {
  const {
    player
  } = useContext(playContext)

  const giving = from.id === player.id

  const preposition = giving ? 'to' : 'from'

  const name = giving ? taker : giver

  const yes = { offerId: id, accept: true }
  const no = { offerId: id }

  const functions = giving
    ? <Function
      name='trade'
      data={no}
      label='Retract'
    />
    : [
      <Function
        name='trade'
        data={yes}
        label='Accept'
      />,
      <Function
        name='trade'
        data={no}
        label='Reject'
      />
    ]

  return <Card inline>
    <p>
      {amount}
      {' '}
      {preposition}
      {' '}
      <b>{name}</b>
    </p>

    {functions}
  </Card>
}
