import React from 'react'

import Exchange from './Exchange'
import Offered from './Offered'

export default function Trade ({ where }) {
  return <Exchange
    collection='offer'
    View={Offered}
    where={where}
  />
}
