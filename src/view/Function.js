import React from 'react'

import useFunction from '../use/function'

import Form from './Form'

export default function Function ({
  children,
  data,
  end,
  label,
  onDone,
  name,
  set,
  show,
  start
}) {
  const {
    done, failed, run, loading, status
  } = useFunction(
    name,
    { data, end, label, onDone, set, start }
  )

  return <Form
    done={done}
    failed={failed}
    loading={loading}
    onSubmit={run}
    name={label || name}
    status={status}
    show={show}
  >
    {children}
  </Form>
}
