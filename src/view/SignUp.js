import React from 'react'

import useAuthForm from '../use/auth/form'

import Field from './Field'
import Function from './Function'

export default function SignUp () {
  const {
    email,
    name,
    password,
    setEmail,
    setName,
    setPassword
  } = useAuthForm()

  const data = { email, name, password }

  return <Function
    data={data}
    end='Signed up!'
    label='Sign up'
    name='signUp'
    start='Signing up...'
  >
    <Field
      placeholder='Email'
      onChange={setEmail}
      value={email}
    />

    <Field
      placeholder='Name'
      onChange={setName}
      value={name}
    />

    <Field
      placeholder='Password'
      onChange={setPassword}
      value={password}
    />
  </Function>
}
