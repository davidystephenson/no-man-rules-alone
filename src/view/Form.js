import React from 'react'

export default function Form ({
  children,
  done,
  failed,
  loading,
  onSubmit,
  name,
  show,
  status
}) {
  const className = done
    ? 'done'
    : failed
      ? 'failed'
      : null

  const span = <span className={className}>
    {status}
  </span>

  const message = status
    ? done
      ? show
        ? span
        : null
      : span
    : null

  const button = <button disabled={loading}>
    {name}
  </button>

  function submit (event) {
    event.preventDefault()

    onSubmit()
  }

  return <form onSubmit={submit}>
    {children} {button} {message}
  </form>
}
