import React, { useContext } from 'react'

import fireContext from '../context/fire'
import playContext from '../context/play'

import useCollection from '../use/collection'

import Viewer from './Viewer'

export default function Exchange ({
  collection, debug, View, where
}) {
  const { store } = useContext(fireContext)
  const { ref } = useContext(playContext)

  const offered = store
    .collection(collection)
    .where(where, '==', ref)

  const stream = useCollection(offered)

  if (debug) {
    console.debug('where test:', where)
    console.debug('stream test:', stream)
  }

  return <Viewer
    stream={stream}
    View={View}
  />
}
