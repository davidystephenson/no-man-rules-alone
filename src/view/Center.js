import React, {
  Fragment, useContext
} from 'react'

import gameContext from '../context/game'

export default function Center () {
  const {
    left,
    homeland,
    right,
    tax,
    treasury,
    victory
  } = useContext(gameContext)

  return <Fragment>
    <table>
      <thead>
        <tr>
          <th>Treasury</th>
          <th>Tax</th>
          <th>Neighbor</th>
          <th>Homeland</th>
          <th>Neighbor</th>
          <th>Victory</th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <td>{treasury}</td>
          <td>{tax}</td>
          <td>{left}</td>
          <td>{homeland}</td>
          <td>{right}</td>
          <td>{victory}</td>
        </tr>
      </tbody>
    </table>
  </Fragment>
}
