import React, { useContext } from 'react'

import gameContext from '../context/game'

import Function from './Function'

export default function Move ({
  end,
  name,
  start
}) {
  const { id } = useContext(gameContext)

  const data = { id }

  return <Function
    data={data}
    end={end}
    name={name}
    start={start}
  />
}
