// TODO separate fragment and useContext?
import React, { useContext } from 'react'

import playContext from '../context/play'

import Function from './Function'

export default function Client ({ player }) {
  const {
    game: { phase },
    leader: { pledged },
    leaderId,
    playerId
  } = useContext(playContext)

  const data = { id: player.id }
  const give = { ...data, give: true }

  const following = player.id !== leaderId
  const leading = playerId === leaderId
  const time = phase === 'patronage'

  const patronizing = following &&
    leading &&
    time

  const patrons = []

  if (patronizing) {
    if (pledged) {
      patrons.push(<Function
        data={give}
        name='patronize'
        start='Giving...'
        end='Given!'
        label='Give'
      />)
    }

    if (player.pledged) {
      patrons.push(<Function
        data={data}
        name='patronize'
        start='Taking...'
        end='Taken!'
        label='Take'
      />)
    }
  }

  const treasure = player.pledged || 0

  return <td>{treasure} {patrons}</td>
}
