import React from 'react'
import { Link } from 'react-router-dom'

export default function Title (
  { children, h = 1, to = '/' }
) {
  const Element = `h${h}`

  return <Element>
    <Link to={to}>
      {children}
    </Link>
  </Element>
}
