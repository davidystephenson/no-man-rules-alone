import React, { useContext } from 'react'

import gameContext from '../context/game'
import playContext from '../context/play'

import Function from './Function'

export default function Remove ({
  armies, name
}) {
  const {
    id, leader
  } = useContext(gameContext)
  const {
    id: playerId
  } = useContext(playContext)

  const data = { id, nation: name }

  const occupied = armies > 0
  const leading = leader.id === playerId
  const removable = occupied && leading

  const remove = removable && <Function
    data={data}
    name='Remove'
    start='Removing...'
    end='Removed!'
  />

  return <td><h4>{armies}</h4> {remove}</td>
}
