import React, { useContext } from 'react'

import playContext from '../context/play'

import Agendas from './Agendas'
import Influences from './Influences'

export default function Hand () {
  const { player } = useContext(playContext)

  return [
    <h3 key='title'>Hand</h3>,
    <div key='body' className='hand'>
      <div className='hand'>
        <Influences player={player} hand />
      </div>

      <div className='hand'>
        <Agendas />
      </div>
    </div>
  ]
}
