import React, {
  Fragment, useContext
} from 'react'

import playContext from '../context/play'

import Deal from './Deal'
import Ready from './Ready'

export default function Deals ({ where }) {
  const { player } = useContext(playContext)

  if (!player) {
    return null
  }

  return <Fragment>
    <Ready />

    <Deal where='to' />

    <Deal where='from' />
  </Fragment>
}
