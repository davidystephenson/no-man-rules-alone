import React, { useContext } from 'react'

import { alphabetize } from '../do'

import gameContext from '../context/game'
import fireContext from '../context/fire'
import playContext from '../context/play'

import useAuth from '../use/auth'

import Aid from './Aid'
import Area from './Area'
import Log from './Log'
import Trades from './Trades'
import Phase from './Phase'
import Players from './Players'
import User from './User'

import './play.css'

export default function Play ({ players }) {
  players.sort(alphabetize)

  const { store } = useContext(fireContext)
  const game = useContext(gameContext)

  const { user: { uid } } = useAuth()

  const ids = players.map(player => player.id)
  const refs = ids.map(id => store
    .collection('player')
    .doc(id)
  )

  const player = players
    .find(data => data.name === uid)
  const ref = player && store
    .collection('player')
    .doc(player.id)
  const id = player && player.id

  const leaderRef = game.leader
  const leaderId = leaderRef && leaderRef.id
  const leader = leaderId && players
    .find(player => player.id === leaderId)

  const data = {
    current: player,
    currentRef: ref,
    currentData: player,
    currentId: id,
    game, // TODO snap,
    gameData: game,
    gameId: game.id,
    gameRef: game.ref,
    id,
    ids,
    leader, // TODO snap
    leaderData: leader,
    leaderId,
    leaderRef,
    player, // TODO snap
    playerData: player,
    playerId: id,
    players, // TODO snaps
    playerIds: ids,
    playerRef: ref,
    playerRefs: refs,
    ref,
    refs
  }

  const { Provider } = playContext

  const user = !player && <User />

  return <Provider value={data}>
    <Players />

    <Log />

    <Trades />

    <Phase />

    <Area />

    {user}

    <Aid />
  </Provider>
}
