import React, { useContext } from 'react'

import gameContext from '../context/game'
import playContext from '../context/play'
import playerContext from '../context/player'

export default function Supporter () {
  const { phase } = useContext(gameContext)
  const {
    id, support
  } = useContext(playerContext)
  const {
    id: playerId
  } = useContext(playContext)

  const is = <td>{support}</td>

  if (phase === 'rebel') {
    if (support) {
      if (id === playerId) {
        return is
      }

      return <td>???</td>
    }

    return <td />
  }

  if (support) {
    return is
  }

  return <td>Supporter</td>
}
