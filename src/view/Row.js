import React from 'react'

import useAgendas from '../use/agendas'

import Count from './Count'
import Name from './Name'
import Player from './Player'
import Supporter from './Supporter'

export default function Row (player) {
  const { hand, done } = useAgendas(player)

  const { treasure, points } = player

  const agendas = <Count items={hand} />

  const achieved = done.join(', ')
  return <Player {...player} >
    <tr>
      <td><Name /></td>
      <td>{treasure}</td>
      <Supporter />
      <td>{agendas}</td>
      <td className='achieved'>
        {achieved}
      </td>
      <td>{points}</td>
    </tr>
  </Player>
}
