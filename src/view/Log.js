import React, { useContext } from 'react'

import playContext from '../context/play'

export default function Log () {
  const {
    gameData: { log }
  } = useContext(playContext)

  if (!log) {
    return null
  }

  const reversed = [...log].reverse()

  const messages = reversed
    .map((message, index) =>
      <p key={index}>{message}</p>
    )

  return <div className='log'>{messages}</div>
}
