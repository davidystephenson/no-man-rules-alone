import React, { useContext } from 'react'

import playContext from '../context/play'

import Client from './Client'

export default function Patrons () {
  const { players } = useContext(playContext)

  const heads = players.map(
    player => <th key={player.id}>
      {player.name}
    </th>
  )

  const clients = players.map(player => <Client
    key={player.id} player={player}
  />)

  return <table>
    <thead>
      <tr>
        {heads}
      </tr>
    </thead>
    <tbody>
      <tr>
        {clients}
      </tr>
    </tbody>
  </table>
}
