import React from 'react'

import Function from './Function'

export default function Appoint ({
  id, title
}) {
  const data = { id, title }

  return <Function
    data={data}
    end='Appointed!'
    label={title}
    name='Appoint'
    start='Appointing...'
  />
}
