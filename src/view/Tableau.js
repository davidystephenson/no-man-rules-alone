import React, { Fragment } from 'react'

import Influences from './Influences'

export default function Tableau ({
  player
}) {
  const { name, agree } = player

  const label = agree
    ? `${name} (Agreed)`
    : name

  return <Fragment>
    <h4>{label}</h4>

    <Influences player={player} />
  </Fragment>
}
