import React, {
  Fragment, useContext, useState
} from 'react'

import playContext from '../context/play'

import Card from './Card'
import Offer from './Offer'
import Trade from './Trade'

export default function Trades ({ where }) {
  const [amount, setAmount] = useState(0)

  const {
    player, players
  } = useContext(playContext)

  if (!player) {
    return null
  }

  const { name } = player

  function set ({ target: { value } }) {
    setAmount(value)
  }

  // TODO automatic label punctuation
  const offers = players
    .filter(player => player.name !== name)
    .map(player => <Offer
      key={player.id}
      to={player}
      amount={amount}
    />)

  return <Fragment>
    <h3>Trades</h3>

    <Card inline>
      <input
        type='number'
        onChange={set}
        value={amount}
        placeholder='amount'
      />

      {offers}
    </Card>

    <Trade where='to' />

    <Trade where='from' />
  </Fragment>
}
