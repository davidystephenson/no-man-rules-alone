import React from 'react'

import { Link } from 'react-router-dom'

import Id from './Id'

export default function GameLink ({ id }) {
  const path = `/game/${id}`

  return <Link to={path}>
    <Id id={id} />
  </Link>
}
