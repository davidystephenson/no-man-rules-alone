import React, {
  useContext
} from 'react'

import fireContext from '../context/fire'
import gameContext from '../context/game'

import useWhere from '../use/where'

import Center from './Center'
import Play from './Play'
import Viewer from './Viewer'

export default function Gameplay (game) {
  const { store } = useContext(fireContext)

  const ref = store
    .collection('game')
    .doc(game.id)

  const players = useWhere(
    'player', 'game', '==', ref
  )

  const value = { ref, gameRef: ref, ...game }

  const { Provider } = gameContext

  return <Provider value={value}>
    <Center />

    <Viewer
      stream={players}
      View={Play}
      prop='players'
    />
  </Provider>
}
