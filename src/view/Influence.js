import React, { useContext } from 'react'

import playContext from '../context/play'

import Card from './Card'
import Pledge from './Pledge'

export default function Influence ({
  hand, id, name, owner, player, to
}) {
  const {
    currentId, players
  } = useContext(playContext)

  const mine = currentId &&
    (
      currentId === player.id ||
      owner.id === currentId
    )

  const targets = hand
    ? players
    : players
      .filter(player => player.id !== owner.id)

  const pledges = mine
    ? targets.map(player =>
      <Pledge
        key={player.id}
        id={id}
        to={player}
        show={to && to.id === player.id}
      />
    )
    : null

  return <Card inline>
    {name} {pledges}
  </Card>
}
