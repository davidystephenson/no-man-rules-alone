import React, { useContext } from 'react'

import playContext from '../context/play'

import Function from './Function'

export default function Ready () {
  const {
    id, playerData
  } = useContext(playContext)

  const labels = playerData.agree
    ? [
      'Disagree',
      'Disagreeing...',
      'Disagreed!'
    ]
    : [
      'Agree', 'Agreeing...', 'Agreed!'
    ]

  const [label, start, end] = labels

  const data = { id }

  return playerData && <Function
    data={data}
    end={end}
    name='ready'
    label={label}
    start={start}
  />
}
