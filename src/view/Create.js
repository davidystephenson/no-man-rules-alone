import React, { useState } from 'react'

import Field from './Field'
import Function from './Function'

export default function Create () {
  const [id, setId] = useState('')

  const data = { id }

  function onDone () {
    setId('')
  }

  return <Function
    data={data}
    end='Created!'
    onDone={onDone}
    name='Create'
    set={setId}
    start='Creating...'
  >
    <Field
      value={id}
      onChange={setId}
      placeholder='game'
    />
  </Function>
}
