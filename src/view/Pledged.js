import React, {
  Fragment, useContext
} from 'react'

import playContext from '../context/play'

import Card from './Card'
import Function from './Function'

export default function Pledged ({
  from, giver, name, id, hand, out, taker
}) {
  const { player } = useContext(playContext)

  const giving = from.id === player.id

  const preposition = giving ? 'to' : 'from'

  const partner = giving ? taker : giver
  const label = !hand || giving
    ? <Fragment>
      {preposition}
      {' '}
      <b>{partner}</b>
      {' '}
    </Fragment>
    : null

  const yes = { id }
  const no = { id, answer: 'no' }
  const location = hand ? 'hand' : 'tableau'

  const functions = giving
    ? <Function
      name='accept'
      data={no}
      label='Retract'
    />
    : <div className='inline'>
      <Function
        name='Accept'
        data={yes}
      />

      <Function
        name='accept'
        data={no}
        label='Reject'
      />
    </div>

  return <Card inline>
    <p>
      {name}
      {' '}
      {label}
      out of
      {' '}
      <b>{out}</b>'s
      {' '}
      {location}
    </p>

    {functions}
  </Card>
}
