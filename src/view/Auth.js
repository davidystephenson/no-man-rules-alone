import React, { Fragment } from 'react'

import useAuth from '../use/auth'

import SignIn from './SignIn'
import SignUp from './SignUp'

export default function Auth () {
  const { loading, user } = useAuth()

  if (loading || user) {
    return null
  }

  return <Fragment>
    <SignUp />

    <SignIn />
  </Fragment>
}
