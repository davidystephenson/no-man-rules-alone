import React from 'react'

import useSignIn from '../use/signIn'

import Field from './Field'
import Form from './Form'

export default function SignIn () {
  const {
    act,
    done,
    email,
    failed,
    loading,
    password,
    setEmail,
    setPassword,
    status
  } = useSignIn()

  return <Form
    done={done}
    failed={failed}
    loading={loading}
    name={'Sign in'}
    onSubmit={act}
    status={status}
  >
    <Field
      placeholder='Email'
      onChange={setEmail}
      value={email}
    />

    <Field
      placeholder='Password'
      onChange={setPassword}
      value={password}
    />
  </Form>
}
