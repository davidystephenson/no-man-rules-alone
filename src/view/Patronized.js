import React, { useContext } from 'react'

import gameContext from '../context/game'
import playContext from '../context/play'

import Function from './Function'

export default function Government () {
  const {
    id, leader
  } = useContext(gameContext)
  const { player } = useContext(playContext)

  const data = { id }

  if (player && leader.id === player.id) {
    return <Function
      data={data}
      name='patronized'
      start='...'
      end='!'
      label='Done'
    />
  }

  return null
}
