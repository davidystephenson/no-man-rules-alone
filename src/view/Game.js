import React, {
  Fragment, useContext
} from 'react'

import fireContext from '../context/fire'

import useDoc from '../use/doc'

import Gameplay from './Gameplay'
import Title from './Title'
import Viewer from './Viewer'

export default function Game (
  { match: { url, params: { id } } }
) {
  const { store } = useContext(fireContext)

  const ref = store.doc(url)

  const stream = useDoc(ref)

  return <Fragment>
    <Title h='2' to={url}>{id}</Title>

    <Viewer stream={stream} View={Gameplay} />
  </Fragment>
}
