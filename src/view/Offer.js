import React, { useContext } from 'react'

import playContext from '../context/play'

import Function from './Function'

export default function Offer ({
  to, amount
}) {
  const { player } = useContext(playContext)

  const data = { toId: to.id, amount }

  return player && <Function
    name='Offer'
    data={data}
    label={to.name}
    end='Offered!'
    start='Offering...'
  />
}
