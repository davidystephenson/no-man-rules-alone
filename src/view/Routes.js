import React from 'react'
import { Route } from 'react-router-dom'

import Game from './Game'
import Games from './Games'

export default function Routes () {
  return <main>
    <Route
      exact path='/' component={Games}
    />

    <Route
      path='/game/:id' component={Game}
    />
  </main>
}
