import React, {
  Fragment, useContext
} from 'react'

import playContext from '../context/play'

import Row from './Row'

export default function Players () {
  const { players } = useContext(playContext)

  const rows = players
    .map(player => <Row
      key={player.id} {...player}
    />)

  return <Fragment>
    <h3>Players</h3>

    <table className='players'>
      <thead>
        <tr>
          <th>Name</th>
          <th>Treasure</th>
          <th>Support</th>
          <th>Agendas</th>
          <th>Achieved</th>
          <th>Points</th>
        </tr>
      </thead>

      <tbody>{rows}</tbody>
    </table>
  </Fragment>
}
