import React, { useContext } from 'react'

import playContext from '../context/play'

import Contribute from './Contribute'

export default function Contribution () {
  const { player } = useContext(playContext)

  if (player) {
    const { treasure } = player

    const zero = <Contribute campaign={0} />

    const contributes = Array(treasure)
      .fill()
      .map((_, index) => <Contribute
        key={index} campaign={index + 1}
      />)

    return [zero, ...contributes]
  }

  return null
}
