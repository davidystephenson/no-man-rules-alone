import React, { useContext } from 'react'

import gameContext from '../context/game'
import playContext from '../context/play'

import Appoint from './Appoint'

export default function Candidate ({
  titles, player
}) {
  const {
    leader, freedom, peace, plenty
  } = useContext(gameContext)

  const {
    player: current
  } = useContext(playContext)

  const roles = [
    leader, freedom, peace, plenty
  ]
  const ids = []
  roles
    .forEach(role => role && ids.push(role.id))

  if (
    titles[0] !== 'Leader' &&
    ids.includes(player.id)
  ) {
    return null
  }

  const name = player.name
    ? player.name
    : player.data().name

  const leading = leader &&
    current &&
    leader.id === current.id

  const appointments = leading && titles
    .map(title => <Appoint
      id={player.id}
      key={title}
      title={title}
    />)

  return <div className='inline'>
    {name} {appointments}
  </div>
}
