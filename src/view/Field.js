import React from 'react'

export default function Field ({
  label,
  type = 'text',
  onChange,
  placeholder,
  value
}) {
  function set ({ target: { value } }) {
    onChange(value)
  }

  return <div>
    <label>
      {label}

      <input
        type={type}
        onChange={set}
        placeholder={placeholder}
        value={value}
      />
    </label>
  </div>
}
