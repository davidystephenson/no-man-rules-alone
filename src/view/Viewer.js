import React, { Fragment } from 'react'

export default function Viewer ({
  debug,
  prop,
  stream,
  View,
  Wrapper = Fragment
}) {
  if (debug) {
    console.debug('stream test:', stream)
  }

  const [ data, firing, error ] = stream

  if (firing) {
    return <Wrapper>Loading...</Wrapper>
  }

  if (error) {
    return <Wrapper>Error: {error}</Wrapper>
  }

  if (data) {
    if (debug) {
      console.debug('data test:', data)
    }

    if (View) {
      if (prop) {
        const props = { [prop]: data }
        return <View {...props} />
      }

      if (Array.isArray(data)) {
        const items = data.map(datum => <View
          key={datum.id}
          {...datum}
        />)

        if (debug) {
          console.debug('items test:', items)
        }

        return <Fragment>
          {items}
        </Fragment>
      }

      return <View {...data} />
    }

    console.warn(
      'No View was provided for data:',
      data
    )
  }

  return null
}
