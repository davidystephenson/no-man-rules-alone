import React, { useContext } from 'react'

import gameContext from '../context/game'

import Function from './Function'

export default function Contribute ({
  campaign
}) {
  const { id } = useContext(gameContext)

  const data = { id, campaign }

  const label = campaign || '0'

  return <Function name='Campaign'
    data={data}
    label={label}
    start='Campaigning...'
    end='Campaigned!'
  />
}
