import React, { useContext } from 'react'

import playContext from '../context/play'

import Tableau from './Tableau'

export default function Tableaus () {
  const { players } = useContext(playContext)

  return players.map(player => <Tableau
    key={player.id} player={player}
  />)
}
