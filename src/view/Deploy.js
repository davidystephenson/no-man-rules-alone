import React, { useContext } from 'react'

import gameContext from '../context/game'
import playContext from '../context/play'

import Function from './Function'

export default function Deploy ({
  armies, name
}) {
  const {
    id, freedom
  } = useContext(gameContext)
  const {
    id: playerId
  } = useContext(playContext)

  const minister = freedom.id === playerId

  const options = []

  if (minister) {
    if (armies > 0) {
      const data = {
        id, nation: name, amount: -1
      }

      options.push(<Function
        data={data}
        key='remove'
        name='Freedom'
        label='Remove'
        start='Removing...'
        end='Removed!'
      />)
    }

    if (name === 'homeland') {
      const amounts = [1, 2, 3]
      amounts.forEach(amount => {
        const data = {
          id, nation: name, amount
        }

        const label = `Add ${amount}`

        options.push(<Function
          data={data}
          key='remove'
          name='Freedom'
          label={label}
          start='Adding...'
          end='Added!'
        />)
      })
    }
  }

  return <td><h4>{armies}</h4> {options}</td>
}
