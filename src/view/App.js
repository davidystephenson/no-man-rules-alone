import React from 'react'
import {
  BrowserRouter
} from 'react-router-dom'

import useAuth from '../use/auth'

import Header from './Header'
import Routes from './Routes'
import Viewer from './Viewer'

import './reset.css'
import './brutal.css'

export default function App () {
  const { state } = useAuth()

  return <BrowserRouter>
    <Header />

    <Viewer stream={state} View={Routes} />
  </BrowserRouter>
}
