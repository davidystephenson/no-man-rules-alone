import React, { useContext } from 'react'

import fireContext from '../context/fire'
import playerContext from '../context/player'

export default function Player (props) {
  const { store } = useContext(fireContext)
  const { Provider } = playerContext

  const value = { ...props }

  const ref = store
    .collection('player')
    .doc(props.id)

  value.ref = ref
  value.playerRef = ref

  return <Provider value={value}>
    {props.children}
  </Provider>
}
