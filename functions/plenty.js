const {
  firestore: { FieldValue }
} = require('firebase-admin')

const checkCurrent = require('./check/current')
const checkGame = require('./check/game')
const checkPlayer = require('./check/player')

const adjourn = require('./adjourn')
const error = require('./error')
const upset = require('./upset')

module.exports = async function Plenty (
  { id, change }, { auth: { uid } }
) {
  try {
    await checkCurrent(uid)

    // TODO replace checkDocs
    const {
      ref: game, data: { tax, plenty }
    } = await checkGame(id)
    const {
      id: playerId
    } = await checkPlayer(uid, game)

    if (playerId !== plenty.id) {
      return error(
        'You are not the Minister of Plenty'
      )
    }
    // TODO check change

    const rate = tax + parseInt(change)

    if (rate > 4) {
      return error(
        'The tax rate is high enough'
      )
    }

    if (rate < 0) {
      return error(
        'The tax rate is low enough'
      )
    }

    await upset(game, {
      tax: FieldValue.increment(change)
    })

    return adjourn(id)
  } catch (error) {
    console.error(error)
    throw error
  }
}
