const checkGame = require('./check/game')

const upset = require('./upset')

module.exports = async function Adjourn (id) {
  try {
    const {
      ref: game,
      data: { freedom, leader, peace, plenty }
    } = await checkGame(id)

    const phase = freedom && peace && plenty
      ? 'victory'
      : 'government'

    return upset(game, { phase, turn: leader })
  } catch (error) {
    console.error(error)
    throw error
  }
}
