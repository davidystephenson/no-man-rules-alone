const admin = require('firebase-admin')
const {
  config
} = require('firebase-functions')

const { firebase } = config()
admin.initializeApp(firebase)

module.exports = admin.firestore()
