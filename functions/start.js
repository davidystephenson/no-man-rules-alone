const checkPlay = require('./check/play')

const error = require('./error')
const upset = require('./upset')

module.exports = async function start (
  { id }, { auth: { uid } }
) {
  try {
    const {
      gameRef,
      gameData: { phase },
      log,
      playerRefs
    } = await checkPlay(uid, { gameId: id })

    if (phase !== 'join') {
      return error(
        'The game is not waiting to start'
      )
    }

    log(`${uid} started the game`)

    const homeland = playerRefs.length - 2

    return upset(gameRef, {
      phase: 'lead',
      homeland,
      left: 1,
      right: 1
    })
  } catch (error) {
    throw error
  }
}
