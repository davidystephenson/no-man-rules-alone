const checkCurrent = require('./check/current')
const checkGame = require('./check/game')
const checkPlayers = require('./check/players')

const Batch = require('./Batch')
const error = require('./error')

module.exports = async function patronized (
  { id }, { auth: { uid } }
) {
  try {
    await checkCurrent(uid)

    const {
      ref: game, data: { leader }
    } = await checkGame(id)

    const {
      player, players
    } = await checkPlayers(game, uid)

    if (leader.id !== player.id) {
      return error(
        'You are not the Leader'
      )
    }

    const batch = Batch()

    batch.ups(players, { support: false })
    batch.up(game, { phase: 'rebel' })

    return batch.go()
  } catch (error) {
    console.error(error)

    throw error
  }
}
