const timestamp = require('./timestamp')

module.exports = async function upset (
  doc, data = {}, debug
) {
  if (debug) {
    console.debug('doc test:', doc)
    console.debug('data test:', data)
  }

  try {
    const snapshot = await doc.get()

    const timestamped = timestamp(
      data, snapshot.exists
    )

    if (snapshot.exists) {
      return doc.update(timestamped)
    } else {
      return doc.set(timestamped)
    }
  } catch (error) {
    throw error
  }
}
