const checkDoc = require('./doc')

module.exports = async function checkBit (
  id, type, { collection, explanation, name }
) {
  collection = collection ||
    type.toLowerCase()

  name = name || type

  return checkDoc(
    id, collection, { explanation, name }
  )
}
