const checkDoc = require('./doc')

module.exports = async function checkUser (
  uid, { explanation, name } = {}
) {
  try {
    return checkDoc(
      uid, 'user', { explanation, name }
    )
  } catch (error) {
    throw error
  }
}
