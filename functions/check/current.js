const checkUser = require('./user')

module.exports = async function checkCurrent (
  uid
) {
  try {
    return checkUser(
      uid,
      'current user',
      'Are you logged in?'
    )
  } catch (error) {
    throw error
  }
}
