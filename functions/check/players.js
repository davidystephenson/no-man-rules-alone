const error = require('../error')
const store = require('../store')

module.exports = async function checkPlayers (
  gameRef, uid
) {
  try {
    const { empty, docs } = await store
      .collection('player')
      .where('game', '==', gameRef)
      .get()

    if (empty) {
      // TODO review category
      return error(
        'No one is playing this game.'
      )
    }

    if (docs.length < 4) {
      return error(
        'You need at least 4 players.'
      )
    }

    const doc = docs
      .find(doc => doc.data().name === uid)

    if (!doc) {
      // TODO add categories
      return error(
        'You are not playing this game.'
      )
    }

    const { ref } = doc

    // TODO functionalize
    const data = doc.data()

    const refs = docs.map(doc => doc.ref)

    const snap = function snap () {
      return { ...doc, data: doc.data() }
    }

    const snaps = function snaps () {
      return docs.map(doc => (
        { ...doc, data: doc.data() }
      ))
    }

    return {
      data,
      id: ref.id,
      player: ref, // TODO change to snap
      playerData: data,
      playerDatas: docs.map(doc => doc.data()),
      playerDoc: doc,
      playerId: ref.id,
      playerRefs: refs,
      players: docs, // TODO change to snaps
      playerSnap: snap,
      playerSnaps: snaps,
      ref,
      refs,
      snap,
      snaps
    }
  } catch (error) {
    throw error
  }
}
