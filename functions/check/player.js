const checkUser = require('./user')

const error = require('../error')
const store = require('../store')

module.exports = async function checkPlayer (
  uid, game
) {
  try {
    const {
      ref: user
    } = await checkUser(uid)

    const { empty, docs } = await store
      .collection('player')
      .where('game', '==', game)
      .where('user', '==', user)
      .get()

    if (empty) {
      // TODO review category
      return error(
        'You are not playing this game'
      )
    }

    if (docs.length > 1) {
      return error(
        'You are more than one player!'
      )
    }

    const [doc] = docs

    // TODO remove data
    return {
      data: doc.data(),
      doc,
      id: doc.id,
      player: doc.ref,
      playerRef: doc.ref,
      ref: doc.ref,
      snap: doc
    }
  } catch (error) {
    throw error
  }
}
