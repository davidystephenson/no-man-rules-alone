const checkBit = require('./bit')

module.exports = async function checkOffer (
  id, options
) {
  return checkBit(id, 'Offer', options)
}
