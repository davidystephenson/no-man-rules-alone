const checkBit = require('./bit')

async function checkInfluence (id, options) {
  return checkBit(id, 'Influence', options)
}

module.exports = checkInfluence
