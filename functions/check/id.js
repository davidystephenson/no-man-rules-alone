const {
  https: { HttpsError }
} = require('firebase-functions')

module.exports = function checkId (id, name) {
  if (!id) {
    throw new HttpsError(
      'invalid-argument', name
    )
  }

  if (typeof id === 'number') {
    id = id.toString()
  }

  const type = typeof id

  if (type !== 'string') {
    const string = JSON.stringify(id)

    throw new HttpsError(
      'invalid-argument',
      `id ${name} was ${type}: ${string}`
    )
  }

  return id
}
