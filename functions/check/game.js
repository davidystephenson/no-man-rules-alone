const {
  firestore: { FieldValue }
} = require('firebase-admin')

const checkDoc = require('./doc')

const upset = require('../upset')

module.exports = async function checkGame (
  id, { explanation, name } = {}
) {
  name = name || 'Game'

  const checked = await checkDoc(
    id, 'game', { explanation, name }
  )

  // TODO remove references
  checked.game = checked.ref

  checked.log = async function log (message) {
    return upset(checked.ref, {
      log: FieldValue.arrayUnion(message)
    })
  }

  return checked
}
