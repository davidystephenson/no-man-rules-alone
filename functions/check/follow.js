const {
  https: { HttpsError }
} = require('firebase-functions')

const store = require('../store')

const checkUser = require('./user')
const checkCurrent = require('./current')

module.exports = async function checkFollow (
  followerId, followedId, current
) {
  if (followedId === followerId) {
    throw new HttpsError(
      'failed-precondition',
      'You cannot follow yourself.'
    )
  }

  async function check (id, type) {
    if (current === type) {
      return checkCurrent(id, type)
    } else {
      return checkUser(id, type)
    }
  }

  try {
    const followed = await check(
      followedId,
      'followed'
    )
    const follower = await check(
      followerId,
      'follower'
    )

    const id = `${follower.id}_${followed.id}`
    const doc = store
      .collection('follow')
      .doc(id)
    const follow = await doc.get()

    return {
      doc,
      followed,
      follower,
      follows: follow.exists
    }
  } catch (error) {
    throw error
  }
}
