const checkCurrent = require('./current')
const checkGame = require('./game')
const checkInfluence = require('./influence')
const checkOffer = require('./offer')
const checkPlayer = require('./player')
const checkPlayers = require('./players')

const error = require('../error')

async function check (id, checker) {
  if (id) {
    return checker(id)
  }

  return {}
}

function identify (snap) {
  if (snap && snap.data && snap.data.game) {
    return snap.data.game.id
  }

  return null
}

module.exports = async function checkPlay (
  uid,
  { gameId, influenceId, offerId, playerId },
  debug
) {
  if (debug) {
    console.debug('uid debug:', uid)
    console.debug('gameId debug:', gameId)
  }

  await checkCurrent(uid)

  if (
    !gameId &&
    !influenceId &&
    !offerId &&
    !playerId
  ) {
    return error('What game are you playing?')
  }

  const player = await check(
    playerId, checkPlayer
  )

  const influence = await check(
    influenceId, checkInfluence
  )

  const offer = await check(
    offerId, checkOffer
  )

  gameId = gameId ||
    identify(player) ||
    identify(influence) ||
    identify(offer)

  if (debug) {
    console
      .debug('gameId after debug:', gameId)
  }

  if (!gameId) {
    return error('A checker did not work')
  }

  const game = await checkGame(gameId)

  if (debug) {
    console.debug('game debug:', game)
    console.debug('game.ref debug:', game.ref)
  }

  const players = await checkPlayers(
    game.ref, uid
  )

  return {
    ...game,
    ...influence,
    ...offer,
    ...players,
    ...player
  }
}
