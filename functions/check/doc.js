const checkId = require('./id')

const error = require('../error')
const store = require('../store')

module.exports = async function checkDoc (
  id, collection, { explanation, name } = {}
) {
  name = name || collection
  id = checkId(id, name)

  // TODO checkCollection
  if (typeof collection === 'string') {
    collection = store.collection(collection)
  }

  const ref = collection.doc(id)

  try {
    const doc = await ref.get()

    if (!doc.exists) {
      const statement = `${name} not found.`
      const message = explanation
        ? `${statement} ${explanation}`
        : statement

      return error(message, 'not-found')
    }

    const data = doc.data()

    return {
      data,
      doc,
      id,
      [`${collection.id}Data`]: data,
      [`${collection.id}Id`]: id,
      [`${collection.id}Ref`]: ref,
      ref
    }
  } catch (error) {
    console.error(error)

    throw error
  }
}
