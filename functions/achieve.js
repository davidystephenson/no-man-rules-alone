const checkCurrent = require('./check/current')
const checkGame = require('./check/game')
const checkPlayer = require('./check/player')

const Batch = require('./Batch')
const error = require('./error')

module.exports = async function achieve (
  { id, name, points }, { auth: { uid } }
) {
  try {
    await checkCurrent(uid)

    // TODO replace checkDocs
    const {
      ref: game, doc: gameDoc
    } = await checkGame(id)
    const {
      ref: player, doc: playerDoc
    } = await checkPlayer(uid, game)

    const { turn } = gameDoc.data()

    // TODO checkTurn function
    if (player.id !== turn.id) {
      return error('It is not your turn')
    }

    const batch = Batch()

    const key = `achieved.${name}`
    batch.up(game, {
      stay: true,
      [key]: true,
      phase: 'achieved'
    })

    const { points: old } = playerDoc.data()
    const number = parseInt(points)
    batch.up(player, { points: old + number })

    return batch.go()
  } catch (error) {
    console.error(error)
    throw error
  }
}
