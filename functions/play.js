const checkDoc = require('./check/doc')

const Influence = require(
  './model/Influence'
)

const Batch = require('./Batch')
const error = require('./error')
const store = require('./store')

module.exports = async function play (
  { id }, user, uid
) {
  const { ref: game, doc } = await checkDoc(
    id, 'game'
  )

  const player = store
    .collection('player')
    .doc()

  const { agendas } = doc.data()
  const cards = Object.keys(agendas)
  const available = cards
    .filter(card => !agendas[card])

  if (available.length < 5) {
    return error(
      'There are not enough agendas!'
    )
  }

  const shuffled = [...available]
    .sort(() => Math.random() - 0.5)
  const five = shuffled.slice(0, 5)

  const hand = {}
  function draw (card) {
    const key = `agendas.${card}`
    hand[key] = player
  }
  five.forEach(draw)

  const batch = Batch()
  batch.update(game, hand)

  const name = uid
  const influence = Influence({
    game, player, name
  })
  batch.add('influence', influence)

  batch.set(player, {
    game,
    name,
    points: 0,
    support: 'Supporter',
    treasure: 3,
    user,
    uid
  })

  try {
    return batch.commit()
  } catch (error) {
    throw error
  }
}
