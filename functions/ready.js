const checkCurrent = require('./check/current')
const checkDoc = require('./check/doc')
const checkPlayers = require('./check/players')

const crown = require('./crown')
const error = require('./error')
const store = require('./store')
const upset = require('./upset')

module.exports = async function ready (
  { id }, { auth: { uid } }
) {
  try {
    await checkCurrent(uid)

    const {
      ref: player, data: { agree, game, name }
    } = await checkDoc(id, 'player')

    // TODO check other
    if (name !== uid) {
      return error('Speak for yourself')
    }

    await upset(player, { agree: !agree })

    const {
      players
    } = await checkPlayers(game, uid)

    const ready = players
      .every(player => player.data().agree)

    if (ready) {
      const { docs } = await store
        .collection('influence')
        .where('game', '==', game)
        .where('hand', '==', false)
        .get()

      const contest = {}
      docs.forEach(doc => {
        const { owner } = doc.data()

        if (contest[owner.id]) {
          contest[owner.id].push(doc.ref.id)
        } else {
          contest[owner.id] = [doc.ref.id]
        }
      })

      const ids = Object.keys(contest)

      const max = ids.reduce(
        (max, id) => {
          const { length } = contest[id]

          if (length > max) {
            return length
          }

          return max
        },
        0
      )

      const winners = ids.filter(
        id => {
          const { length } = contest[id]

          return length === max
        }
      )

      const { length } = winners

      if (length && length === 1) {
        const [id] = winners

        const winner = store
          .collection('player')
          .doc(id)

        await crown(winner, game, players)
      } else {
        const refs = length === 0
          ? players.map(player => player.ref)
          : winners.map(winner => store
            .collection('player')
            .doc(winner)
          )

        await upset(
          game, { phase: 'tie', winners: refs }
        )
      }
    }
  } catch (error) {
    console.error(error)
    throw error
  }
}
