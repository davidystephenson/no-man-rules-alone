const {
  firestore: { FieldValue }
} = require('firebase-admin')

const checkCurrent = require('./check/current')
const checkDoc = require('./check/doc')
const checkPlayers = require('./check/players')

const Batch = require('./Batch')
const error = require('./error')

module.exports = async function trade (
  { offerId, accept }, { auth: { uid } }
) {
  try {
    await checkCurrent(uid)

    // TODO offer.from
    const {
      ref: offer,
      data: { game, to, amount, player }
    } = await checkDoc(offerId, 'offer')

    const {
      data: { treasure: theirs }
    } = await checkDoc(player.id, 'player')

    let {
      player: current,
      playerId
    } = await checkPlayers(game, uid)

    const ids = [to.id, player.id]
    if (!ids.includes(playerId)) {
      return error(
        'This is not your business'
      )
    }

    const batch = Batch()

    if (accept) {
      if (to.id !== playerId) {
        return error(
          'It must be offered to you'
        )
      }

      if (theirs < amount) {
        return error(
          'They do not have enough treasure'
        )
      }

      batch.up(player, {
        treasure: FieldValue.increment(-amount)
      })

      batch.up(current, {
        treasure: FieldValue.increment(amount)
      })
    }

    batch.remove(offer)

    return batch.go()
  } catch (error) {
    console.error(error)

    throw error
  }
}
