const checkCurrent = require('./check/current')
const checkGame = require('./check/game')
const checkPlayer = require('./check/player')

const error = require('./error')
const upset = require('./upset')

module.exports = async function draw (
  { id, name, points }, { auth: { uid } }
) {
  try {
    await checkCurrent(uid)

    // TODO replace checkDocs
    const {
      ref: game, doc: gameDoc
    } = await checkGame(id)
    const {
      ref: player
    } = await checkPlayer(uid, game)

    const { turn } = gameDoc.data()

    // TODO checkTurn function
    if (player.id !== turn.id) {
      return error('It is not your turn')
    }

    const { agendas } = gameDoc.data()
    const cards = Object.keys(agendas)
      .filter(card => !agendas[card])

    if (!cards.length) {
      return error(
        'There are not enough agendas left!'
      )
    }

    const shuffled = [...cards]
      .sort(() => Math.random() - 0.5)
    const card = shuffled[0]
    const key = `agendas.${card}`

    return upset(game, {
      [key]: player, phase: 'discard'
    })
  } catch (error) {
    console.error(error)
    throw error
  }
}
