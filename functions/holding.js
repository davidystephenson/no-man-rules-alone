module.exports = function holding (
  agendas, player
) {
  const mine = function mine ([name, owner]) {
    return owner && owner.id === player.id
  }

  return Object
    .entries(agendas)
    .filter(mine)
    .map(entry => entry[0])
}
