const error = require('./error')
const tax = require('./tax')
const upset = require('./upset')

function alphabetize (a, b) {
  a = a.data().uid.toLowerCase()
  b = b.data().uid.toLowerCase()

  if (a > b) return 1

  return -1
}

module.exports = async function next (
  gameData, gameRef, player, players, log
) {
  const { leader } = gameData

  players.sort(alphabetize)

  const index = players
    .findIndex(me => me.id === player.id)

  if (index < 0) {
    return error(
      'You are not playing this game'
    )
  }

  const next = (index + 1) % players.length

  const ref = players[next].ref

  try {
    if (ref.id === leader.id) {
      return tax(gameData, gameRef, log)
    }

    return upset(gameRef, {
      turn: ref, phase: 'victory'
    })
  } catch (error) {
    console.error(error)

    throw error
  }
}
