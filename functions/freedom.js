const {
  firestore: { FieldValue }
} = require('firebase-admin')

const checkCurrent = require('./check/current')
const checkGame = require('./check/game')
const checkPlayer = require('./check/player')

const adjourn = require('./adjourn')
const error = require('./error')
const upset = require('./upset')

module.exports = async function Freedom (
  { id, nation, amount }, { auth: { uid } }
) {
  try {
    await checkCurrent(uid)

    // TODO replace checkDocs
    const {
      ref: game, data
    } = await checkGame(id)
    const { freedom } = data
    const {
      id: playerId
    } = await checkPlayer(uid, game)

    // TODO checkTitle function
    if (playerId !== freedom.id) {
      return error(
        'You are not the Minister of Freedom'
      )
    }

    const change = parseInt(amount)
    if (change === 0) {
      return adjourn(id)
    }

    if (change < -1) {
      return error(
        'You can only remove 1 army'
      )
    }

    if (nation === 'homeland') {
      if (change > 3) {
        return error(
          'You can not add more than 3 armies'
        )
      }
    } else {
      if (change > -1) {
        return error(
          'You can only add armies to the homeland'
        )
      }
    }

    await upset(game, {
      [nation]: FieldValue.increment(change)
    })

    return adjourn(id)
  } catch (error) {
    console.error(error)
    throw error
  }
}
