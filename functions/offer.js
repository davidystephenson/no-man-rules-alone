const checkDoc = require('./check/doc')
const checkPlayers = require('./check/players')

const error = require('./error')
const store = require('./store')
const upset = require('./upset')

module.exports = async function offer (
  { toId, amount }, { auth: { uid } }
) {
  // TODO checkNumber({ minimum, maximum })
  amount = amount && parseInt(amount)

  if (amount < 1) {
    return error(
      'You can not trade nothing'
    )
  }

  try {
    const {
      ref: to, data: { game, name: taker }
    } = await checkDoc(toId, 'player')

    let {
      data: { treasure, name: giver }, player
    } = await checkPlayers(game, uid)

    // TODO check other
    if (taker === uid) {
      return error(
        'You can not trade with yourself'
      )
    }

    if (treasure < amount) {
      return error(
        'You do not have that much treasure'
      )
    }

    const offer = store
      .collection('offer')
      .doc()

    // TODO remove offer.player
    return upset(offer, {
      amount,
      from: player,
      game,
      giver,
      player,
      taker,
      to
    })
  } catch (error) {
    console.error('Caught:', error)

    throw error
  }
}
