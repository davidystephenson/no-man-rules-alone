const checkCurrent = require('./check/current')
const checkGame = require('./check/game')
const checkPlayer = require('./check/player')

const adjourn = require('./adjourn')
const error = require('./error')
const upset = require('./upset')

module.exports = async function Peace (
  { id, to, from, armies }, { auth: { uid } }
) {
  try {
    await checkCurrent(uid)

    // TODO replace checkDocs
    const {
      ref: game, data
    } = await checkGame(id)
    const { peace } = data
    const {
      id: playerId
    } = await checkPlayer(uid, game)

    // TODO checkTitle function
    if (playerId !== peace.id) {
      return error(
        'You are not the Minister of Peace'
      )
    }

    const change = parseInt(armies)
    if (change < 1) {
      return adjourn(id)
    }

    const max = data[from]

    if (change > max) {
      return error(
        `There are only ${max} armies there`
      )
    }

    const less = max - change
    const more = data[to] + change

    await upset(game, {
      [from]: less, [to]: more
    })

    return adjourn(id)
  } catch (error) {
    console.error(error)
    throw error
  }
}
