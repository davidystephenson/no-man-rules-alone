const {
  firestore: { FieldValue }
} = require('firebase-admin')

const Batch = require('./Batch')
const store = require('./store')

module.exports = async function tax (
  gameData, gameRef
) {
  try {
    const {
      homeland,
      left,
      leader,
      right,
      stay,
      tax,
      treasury
    } = gameData

    const armies = homeland + left + right

    // TODO reduce revenue
    let revenue = 0
    const { docs: players } = await store
      .collection('player')
      .where('game', '==', gameRef)
      .get()

    const batch = Batch(gameRef)
    players.forEach(doc => {
      const {
        uid, treasure, support
      } = doc.data()

      if (support !== 'Rebel') {
        if (treasure > tax) {
          revenue += tax

          batch.update(doc.ref, {
            treasure: FieldValue
              .increment(-tax)
          })

          batch
            .log(`${uid} payed ${tax} in tax`)
        } else if (treasure > 0) {
          revenue += treasure

          batch.update(doc.ref, {
            treasure: 0
          })

          batch.log(
            `${uid} payed ${treasure} in tax`
          )
        }
      }
    })

    if (!stay) {
      batch.up(gameRef, {
        victory: FieldValue.increment(-1)
      })
    }
    batch.up(gameRef, { stay: false })

    batch.up(leader, { pledged: 10 })

    batch.log(
      `${revenue} treasure was collected in taxes`
    )

    const total = treasury + revenue

    batch.log(
      `The treasury grew to ${total} treasure`
    )

    batch.log(
      `${armies} treasure must be paid for the armies`
    )

    // TODO increment?
    const balance = total - armies

    if (balance >= 0) {
      batch.log(
        `That leaves ${balance} in the treasury`
      )

      batch.up(gameRef, {
        phase: 'patronage', treasury: balance
      })
    } else {
      batch.log(
        `That would leave ${balance} in the treasury`
      )

      batch.up(gameRef, {
        balance, phase: 'tax'
      })
    }

    await batch.commit()
  } catch (error) {
    console.error(error)
    throw error
  }
}
