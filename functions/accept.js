const checkCurrent = require('./check/current')
const checkDoc = require('./check/doc')
const checkPlayer = require('./check/player')

const error = require('./error')
const tableau = require('./tableau')
const upset = require('./upset')

module.exports = async function accept (
  { id, answer = 'yes' }, { auth: { uid } }
) {
  try {
    await checkCurrent(uid)

    const {
      ref: influence,
      data: { from, game, to }
    } = await checkDoc(id, 'influence')

    const {
      id: playerId
    } = await checkPlayer(uid, game)

    const ids = [from.id, to.id]
    if (!ids.includes(playerId)) {
      return error(
        'This is not your business'
      )
    }

    if (answer === 'yes') {
      if (playerId !== to.id) {
        return error(
          'It must be offered to you'
        )
      }

      return tableau(influence, to)
    }

    return upset(influence, {
      from: null, to: null
    })
  } catch (error) {
    console.error(error)
    throw error
  }
}
