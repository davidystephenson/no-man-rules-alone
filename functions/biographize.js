module.exports = function biographize (user) {
  const json = user.toJSON()
  const string = JSON.stringify(json)

  return JSON.parse(string)
}
