const {
  firestore: { FieldValue }
} = require('firebase-admin')

const store = require('./store')
const timestamp = require('./timestamp')

module.exports = function Batch (gameRef) {
  const batch = store.batch()

  function add (collection, data) {
    const ref = store
      .collection(collection)
      .doc()

    set(ref, data)
  }

  function adds (count, collection, data) {
    Array(count)
      .fill(data)
      .forEach(
        datum => add(collection, datum)
      )
  }

  function create (ref, data) {
    when(ref, data, set, sets)
  }

  async function commit () {
    return batch.commit()
  }

  function each (snapshots, data, fn) {
    snapshots.forEach(snapshot =>
      fn(snapshot.ref, data)
    )
  }

  function log (message, debug) {
    if (debug) {
      console.debug('message debug:', message)
    }

    const now = Math.floor(Date.now() / 1000)

    const text = `${message} [${now}]`

    up(gameRef, {
      log: FieldValue.arrayUnion(text)
    }, debug)
  }

  function remove (ref) {
    batch.delete(ref)
  }

  function set (ref, data) {
    data = timestamp(data)

    batch.set(ref, data)
  }

  function sets (snapshots, data) {
    each(snapshots, data, set)
  }

  function update (ref, data, debug) {
    data = timestamp(data, true)

    batch.update(ref, data)

    if (debug) {
      console.debug('update data debug:', data)
      console
        .debug('update ref.id debug:', ref.id)
    }
  }

  function ups (snapshots, data) {
    each(snapshots, data, update)
  }

  function up (ref, data, debug) {
    when(ref, data, update, ups, debug)
  }

  function when (
    ref, data, single, array, debug
  ) {
    if (Array.isArray(ref)) {
      if (debug) {
        console.debug('Batch debug array')
      }

      array(ref, data)
    } else {
      if (debug) {
        console.debug('Batch debug single')
      }

      single(ref, data, debug)
    }
  }

  return {
    add,
    adds,
    batch,
    create,
    commit,
    go: commit,
    log,
    remove,
    set,
    sets,
    update,
    up,
    ups
  }
}
