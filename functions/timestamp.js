const {
  firestore: { FieldValue }
} = require('firebase-admin')

module.exports = function timestamp (
  data = {}, exists
) {
  const time = FieldValue.serverTimestamp()

  const updated = { ...data, updatedAt: time }

  if (exists) {
    return updated
  } else {
    return { ...updated, createdAt: time }
  }
}
