const {
  firestore: { FieldValue }
} = require('firebase-admin')

const checkCurrent = require('./check/current')
const checkDoc = require('./check/doc')
const checkPlayers = require('./check/players')

const Batch = require('./Batch')
const error = require('./error')

module.exports = async function patronize (
  { id, give }, { auth: { uid } }
) {
  try {
    await checkCurrent(uid)

    // TODO make checkPlayer
    const {
      ref: player, data: { game, pledged }
    } = await checkDoc(id, 'player')

    const old = pledged || 0

    const {
      player: me, playerDoc: myDoc
    } = await checkPlayers(game, uid)
    const myData = myDoc.data()
    const { pledged: myPledged } = myData

    const gameDoc = await game.get()
    const gameData = gameDoc.data()
    const { leader } = gameData

    if (leader.id !== me.id) {
      return error('You are not the Leader')
    }

    if (id === me.id) {
      return error(
        'Treasure can not be created or destroyed'
      )
    }

    const batch = Batch()

    if (give) {
      if (myPledged <= 0) {
        return error(
          'You have no treasure to give'
        )
      }
    } else {
      if (old <= 0) {
        return error(
          'They have no treasure to take'
        )
      }
    }

    const mine = give ? -1 : 1
    const theirs = -mine

    batch.up(me, {
      pledged: FieldValue.increment(mine)
    })
    batch.up(player, {
      pledged: FieldValue.increment(theirs)
    })

    return batch.commit()
  } catch (error) {
    console.error(error)
    throw error
  }
}
