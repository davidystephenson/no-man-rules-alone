const {
  firestore: { FieldValue }
} = require('firebase-admin')

const checkCurrent = require('./check/current')
const checkGame = require('./check/game')
const checkPlayers = require('./check/players')

const Batch = require('./Batch')
const error = require('./error')
const upset = require('./upset')

module.exports = async function support (
  { id, support }, { auth: { uid } }
) {
  try {
    await checkCurrent(uid)

    const {
      game, data: { homeland }
    } = await checkGame(id)

    let {
      data: { name }, player
    } = await checkPlayers(game, uid)

    // TODO check other
    if (name !== uid) {
      return error('Speak for yourself')
    }

    await upset(player, { support })

    const {
      players, playerDatas
    } = await checkPlayers(game, uid)

    const batch = Batch()

    const rebels = playerDatas
      .filter(data => data.support === 'Rebel')

    const readies = playerDatas
      .filter(data => data.support)

    if (readies.length >= players.length) {
      batch.ups(players, { pledged: 0 })

      const zero = homeland === 0 &&
        rebels.length === 0

      if (zero || homeland > rebels.length) {
        batch.ups(players, { support: false })

        players.forEach(player => {
          const { pledged } = player.data()

          const change = pledged || 0

          batch.up(player.ref, {
            treasure: FieldValue
              .increment(change)
          })
        })
      }

      batch.up(game, { phase: 'campaign' })
    }

    return batch.go()
  } catch (error) {
    console.error(error)
    throw error
  }
}
