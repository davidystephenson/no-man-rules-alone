const {
  https: { HttpsError }
} = require('firebase-functions')

module.exports = function error (
  message = 'No message provided.',
  category = 'failed-precondition'
) {
  throw new HttpsError(category, message)
}
