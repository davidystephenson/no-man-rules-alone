const upset = require('./upset')

module.exports = async function tableau (
  influence, owner
) {
  try {
    return upset(influence, {
      from: null, hand: false, owner, to: null
    })
  } catch (error) {
    console.error(error)

    throw error
  }
}
