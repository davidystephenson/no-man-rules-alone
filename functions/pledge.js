const checkCurrent = require('./check/current')
const checkDoc = require('./check/doc')
const checkPlayer = require('./check/player')

const error = require('./error')
const tableau = require('./tableau')
const upset = require('./upset')

// TODO toId
module.exports = async function pledge (
  { id, to }, { auth: { uid } }
) {
  try {
    await checkCurrent(uid)

    const {
      ref: player,
      data: {
        name: taker, game: { id: gameId }
      }
    } = await checkDoc(to, 'player')

    const {
      ref: game, data: { phase }
    } = await checkDoc(gameId, 'game')

    const {
      playerRef: from
    } = await checkPlayer(uid, game)

    if (phase !== 'contest') {
      return error('You can not pledge now')
    }

    const {
      ref: influence, data: { owner }
    } = await checkDoc(id, 'influence')

    if (taker === uid) {
      return tableau(influence, player)
    }

    const {
      data: { name: out }
    } = await checkDoc(owner.id, 'player')

    return upset(influence, {
      out, giver: uid, from, to: player, taker
    })
  } catch (error) {
    console.error(error)
    throw error
  }
}
