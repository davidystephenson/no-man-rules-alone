const checkCurrent = require('./check/current')
const checkDoc = require('./check/doc')
const checkPlayers = require('./check/players')

const crown = require('./crown')
const error = require('./error')
const store = require('./store')
const upset = require('./upset')

function Label (title) {
  const upper = title[0].toUpperCase()
  const lower = title.slice(1)

  return `${upper}${lower}`
}

module.exports = async function appoint (
  { id, title }, { auth: { uid } }
) {
  title = title.toLowerCase()

  try {
    await checkCurrent(uid)

    const {
      ref: player, data: { game, name }
    } = await checkDoc(id, 'player')

    const {
      players
    } = await checkPlayers(game, uid)

    const mes = await store
      .collection('player')
      .where('uid', '==', uid)
      .where('game', '==', game)
      .get()
    const [myDoc] = mes.docs

    const gameDoc = await game.get()
    const gameData = gameDoc.data()
    const { leader } = gameData

    if (leader.id !== myDoc.id) {
      return error(
        'You are not the Leader'
      )
    }

    const ministers = [
      'freedom', 'peace', 'plenty'
    ]
    const minister = ministers.includes(title)

    if (minister) {
      if (name === uid) {
        return error(
          'You can not be everywhere at once'
        )
      }

      if (gameData[title]) {
        const label = Label(title)

        return error(
          `You already appointed ${label}`
        )
      }

      const titles = ['leader', ...ministers]
      const already = titles.find(
        title => {
          const current = gameData[title]

          return current &&
            current.id === player.id
        }
      )

      if (already) {
        const label = Label(already)

        return error(
          `They are already ${label}`
        )
      }

      return upset(game, {
        [title]: player, phase: title
      })
    } else if (title === 'leader') {
      return crown(player, game, players)
    } else {
      return error(`What is a ${title}?`)
    }
  } catch (error) {
    console.error(error)
    throw error
  }
}
