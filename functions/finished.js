const checkCurrent = require('./check/current')
const checkGame = require('./check/game')
const checkPlayer = require('./check/player')

const Batch = require('./Batch')
const error = require('./error')

module.exports = async function achieve (
  { id, name, points }, { auth: { uid } }
) {
  try {
    await checkCurrent(uid)

    // TODO replace checkDocs
    const { ref: game } = await checkGame(id)
    const {
      ref: player, doc: playerDoc
    } = await checkPlayer(uid, game)

    // TODO checkTitle function
    if (player.id !== game.turn.id) {
      return error('It is not your turn')
    }

    const batch = Batch()

    const key = `achieved.${name}`
    batch.up(game, {
      [key]: true, phase: 'achieved'
    })

    const { points: old } = playerDoc.data()
    batch.up(player, { points: old + points })

    return batch.go()
  } catch (error) {
    console.error(error)
    throw error
  }
}
