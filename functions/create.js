const error = require('./error')
const play = require('./play')
const store = require('./store')
const upset = require('./upset')

const checkCurrent = require('./check/current')

module.exports = async function create (
  { id }, { auth: { uid } }
) {
  try {
    const {
      ref: user
    } = await checkCurrent(uid)

    const game = store
      .collection('game')
      .doc(id)

    const existing = await game.get()

    if (existing.exists) {
      return error(
        'Another game already has that name'
      )
    }

    // TODO batch
    await upset(game, {
      freedom: null,
      homeland: 0,
      leader: null,
      left: 0,
      plenty: null,
      right: 0,
      phase: 'join',
      tax: 2,
      turn: null,
      victory: 10,
      peace: null,
      winner: null,
      treasury: 0,
      agendas: {
        'A New World Order': null,
        'A Return to Grace': null,
        'Army with a Nation': null,
        'Boom and Bull': null,
        'Classical Feudalism': null,
        'Coup de Tyrant': null,
        'Dead on the Throne': null,
        'Dear Leader': null,
        'Eye of the Needle': null,
        'Eye of the Pyramid': null,
        'Falling Dominoes': null,
        'Father of the Nation': null,
        'Fix Your Windows': null,
        'Hobbes and Calvin': null,
        'Imagine All the People': null,
        'In the Name of Justice': null,
        'Isolation is Bliss': null,
        'Known Unknowns': null,
        'League of Empowered Gentlemen': null,
        'Liberation Accomplished': null,
        'Locke Out': null,
        'Model Modern Major Military': null,
        'Neo-Feudalism': null,
        'One Day More': null,
        'Our Daily Bread': null,
        'Peace Through Occupation': null,
        'Protest Vote': null,
        'Radical Freedom': null,
        'Screw the Law Enforcement Officers': null,
        'Strong and Stable': null,
        'Team of Rivals': null,
        'The Big Stick': null,
        'The King and I': null,
        'The Resistance': null,
        'The Spectre': null,
        'Tower for Tower': null,
        'Troops for the Troop God': null,
        'With Your Shield or on It': null,
        'World on Fire': null,
        'Wormtongue': null,
        'Yes, Minister': null
      },
      achieved: {
        'A New World Order': false,
        'A Return to Grace': false,
        'Army with a Nation': false,
        'Boom and Bull': false,
        'Classical Feudalism': false,
        'Coup de Tyrant': false,
        'Dead on the Throne': false,
        'Dear Leader': false,
        'Eye of the Needle': false,
        'Eye of the Pyramid': false,
        'Falling Dominoes': false,
        'Father of the Nation': false,
        'Fix Your Windows': false,
        'Hobbes and Calvin': false,
        'Imagine All the People': false,
        'In the Name of Justice': false,
        'Isolation is Bliss': false,
        'Known Unknowns': false,
        'League of Empowered Gentlemen': false,
        'Liberation Accomplished': false,
        'Locke Out': false,
        'Model Modern Major Military': false,
        'Neo-Feudalism': false,
        'One Day More': false,
        'Our Daily Bread': false,
        'Peace Through Occupation': false,
        'Protest Vote': false,
        'Radical Freedom': false,
        'Screw the Law Enforcement Officers': false,
        'Strong and Stable': false,
        'Team of Rivals': false,
        'The Big Stick': false,
        'The King and I': false,
        'The Resistance': false,
        'The Spectre': false,
        'Tower for Tower': false,
        'Troops for the Troop God': false,
        'With Your Shield or on It': false,
        'World on Fire': false,
        'Wormtongue': false,
        'Yes, Minister': false
      }
    })

    return play(game, user, uid)
  } catch (error) {
    throw error
  }
}
