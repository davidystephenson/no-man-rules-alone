const {
  firestore: { FieldValue }
} = require('firebase-admin')

const checkCurrent = require('./check/current')
const checkGame = require('./check/game')
const checkPlayers = require('./check/players')

const Batch = require('./Batch')
const error = require('./error')
const Influence = require(
  './model/Influence'
)
const upset = require('./upset')

module.exports = async function campaign (
  { id, campaign }, { auth: { uid } }
) {
  // TODO checkNumber({ minimum, maximum })
  campaign = campaign && parseInt(campaign)

  try {
    await checkCurrent(uid)

    const { game } = await checkGame(id)

    let {
      data: { name, treasure }, player
    } = await checkPlayers(game, uid)

    // TODO check other
    if (name !== uid) {
      return error('Speak for yourself')
    }

    if (treasure < campaign) {
      return error(
        'You do not have that much treasure'
      )
    }

    await upset(player, { campaign })

    const {
      players, playerDatas
    } = await checkPlayers(game, uid)

    const batch = Batch(game)


    const ready = playerDatas.every(data => {
      const number = parseInt(data.campaign)

      return number >= 0
    })

    if (ready) {
      batch.ups(players, { campaign: null })

      const highest = playerDatas.reduce(
        (max, data) => {
          const { campaign, uid } = data

          batch.log(`${uid} bid ${campaign}`)

          return data.campaign > max
            ? data.campaign
            : max
        },
        0
      )

      const winners = players.filter(
        player => {
          return player
            .data()
            .campaign >= highest
        }
      )

      winners.forEach(winner => batch.log(
        `${winner.data().uid} won the campaign`
      ))

      const contribution = campaign

      winners.forEach(winner => {
        const {
          name, campaign
        } = winner.data()

        const cost = campaign || contribution

        batch.up(winner.ref, {
          treasure: FieldValue.increment(-cost)
        })

        const influence = Influence({
          game, player: winner.ref, name
        })

        batch.adds(2, 'influence', influence)
      })

      batch.up(game, { phase: 'contest' })
    }

    return batch.go()
  } catch (error) {
    console.error(error)

    throw error
  }
}
