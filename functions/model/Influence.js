module.exports = function Influence ({
  game, player, name
}) {
  return {
    game,
    hand: true,
    name,
    owner: player,
    player
  }
}
