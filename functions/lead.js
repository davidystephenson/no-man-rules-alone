const checkCurrent = require('./check/current')
const checkDoc = require('./check/doc')
const checkPlayers = require('./check/players')

const error = require('./error')
const upset = require('./upset')

module.exports = async function lead (
  { id }, { auth: { uid } }
) {
  try {
    await checkCurrent(uid)

    const {
      ref: game, doc: gameDoc
    } = await checkDoc(id, 'game')

    const { phase } = gameDoc.data()

    if (phase !== 'lead') {
      return error('Sedition.')
    }

    const {
      player
    } = await checkPlayers(game, uid)

    return upset(game, {
      leader: player, phase: 'contest'
    })
  } catch (error) {
    throw error
  }
}
