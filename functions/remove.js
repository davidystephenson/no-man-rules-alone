const checkCurrent = require('./check/current')
const checkGame = require('./check/game')
const checkPlayer = require('./check/player')

const error = require('./error')
const upset = require('./upset')

module.exports = async function Remove (
  { id, nation }, { auth: { uid } }
) {
  try {
    await checkCurrent(uid)

    // TODO replace checkDocs
    const {
      ref: game, data
    } = await checkGame(id)
    const { balance, leader } = data
    const {
      id: playerId
    } = await checkPlayer(uid, game)

    // TODO checkTitle function
    if (playerId !== leader.id) {
      return error('You are not the Leader')
    }

    const armies = data[nation] - 1

    if (armies < 0) {
      return error('There are no armies there')
    }

    const now = balance + 1

    const remove = {
      [nation]: armies, balance: now
    }

    const update = now === 0
      ? {
        ...remove,
        phase: 'patronage',
        turn: leader
      }
      : remove

    return upset(game, update)
  } catch (error) {
    console.error(error)
    throw error
  }
}
