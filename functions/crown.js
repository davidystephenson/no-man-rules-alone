const Batch = require('./Batch')
const store = require('./store')

module.exports = async function crown (
  winner, game, players
) {
  const batch = Batch()

  const influence = store
    .collection('influence')
    .where('game', '==', game)

  try {
    const all = await influence.get()

    all
      .docs
      .forEach(doc => {
        const {
          hand, owner, player
        } = doc.data()

        const winners = owner.id === winner.id

        if (winners && !hand) {
          batch.remove(doc.ref)
        } else {
          batch.update(doc.ref, {
            hand: true,
            from: null,
            owner: player,
            to: null
          })
        }
      })

    batch.ups(players, { agree: false })

    batch.update(game, {
      freedom: null,
      leader: winner,
      peace: null,
      plenty: null,
      phase: 'government'
    })

    return batch.commit()
  } catch (error) {
    console.error(error)
    throw error
  }
}
