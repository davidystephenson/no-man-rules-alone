const checkCurrent = require('./check/current')
const checkDoc = require('./check/doc')
const checkPlay = require('./check/play')
const checkPlayers = require('./check/players')

const error = require('./error')
const holding = require('./holding')
const next = require('./next')
const store = require('./store')
const upset = require('./upset')

module.exports = async function discard (
  { id, name }, { auth: { uid } }
) {
  try {
    await checkCurrent(uid)

    const {
      gameData, gameRef: game, log
    } = await checkPlay(uid, { gameId: id })

    const { agendas, phase } = gameData

    const phases = ['draft', 'discard']
    if (!phases.includes(phase)) {
      return error(
        `You can not discard during ${phase}`
      )
    }

    const {
      player, players
    } = await checkPlayers(game, uid)

    const hand = holding(agendas, player)

    if (phase === 'draft' && hand.length < 4) {
      return error(
        'You discarded enough agendas'
      )
    }

    if (!hand.includes(name)) {
      return error(
        'You do not have this agenda'
      )
    }

    const key = `agendas.${name}`
    await upset(game, { [key]: null })

    if (phase === 'discard') {
      return next(
        gameData, game, player, players, log
      )
    }

    if (phase === 'draft') {
      const { doc: updated } = await checkDoc(
        id, store.collection('game')
      )

      const { agendas: drawn } = updated.data()

      const every = players.every(player => {
        const hand = holding(drawn, player)

        return hand.length < 4
      })

      if (every) {
        return upset(game, {
          phase: 'contest'
        })
      }
    }
  } catch (error) {
    throw error
  }
}
