const admin = require('firebase-admin')

const biographize = require('./biographize')
const error = require('./error')
const store = require('./store')

module.exports = async function signUp (
  { email, name, password }, { auth }
) {
  if (auth && auth.uid) {
    return error('You are already a user')
  }

  if (name.includes(' ')) {
    return error(
      'Names can not include spaces'
    )
  }

  try {
    const user = store
      .collection('user')
      .doc(name)
    const existing = await user.get()

    if (existing.exists) {
      return error(
        'That name is taken',
        'already-exists'
      )
    }

    const data = {
      email,
      displayName: name,
      uid: name,
      password
    }
    const record = await admin
      .auth()
      .createUser(data)
    const biography = biographize(record)

    return biography
  } catch (error) {
    const emailExists = error.errorInfo &&
      error.errorInfo.code &&
      error
        .errorInfo
        .code === 'auth/email-already-exists'

    if (emailExists) {
      return error(
        'That email is taken',
        'already-exists'
      )
    }

    throw error
  }
}
