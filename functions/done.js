const checkPlay = require('./check/play')

const error = require('./error')
const next = require('./next')

module.exports = async function done (
  { id }, { auth: { uid } }
) {
  try {
    const {
      gameData, gameRef, player, players, log
    } = await checkPlay(uid, { gameId: id })

    // TODO checkTurn function
    const { turn } = gameData
    if (player.id !== turn.id) {
      return error('It is not your turn')
    }

    return next(
      gameData, gameRef, player, players, log
    )
  } catch (error) {
    console.error(error)
    throw error
  }
}
