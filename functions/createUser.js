const admin = require('firebase-admin')
const biographize = require('./biographize')
const store = require('./store')
const upset = require('./upset')

module.exports = async function createUser (
  { uid }
) {
  try {
    const record = await admin
      .auth()
      .getUser(uid)
    const biography = biographize(record)

    const user = store
      .collection('user')
      .doc(uid)

    await upset(user, biography)
  } catch (error) {
    throw error
  }
}
