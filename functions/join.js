const checkCurrent = require('./check/current')
const checkDoc = require('./check/doc')

const error = require('./error')
const play = require('./play')
const store = require('./store')

module.exports = async function join (
  { id }, { auth: { uid } }
) {
  try {
    const {
      ref: user
    } = await checkCurrent(uid)

    const {
      ref: game
    } = await checkDoc(id, 'game')

    const players = await store
      .collection('player')
      .where('game', '==', game)
      .where('user', '==', user)
      .get()

    if (!players.empty) {
      return error(
        'You already joined this game.'
      )
    }

    return play(game, user, uid)
  } catch (error) {
    throw error
  }
}
